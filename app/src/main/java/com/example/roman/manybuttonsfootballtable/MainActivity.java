package com.example.roman.manybuttonsfootballtable;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;


/**
 * Класс {@code MainActivity} предназначен для имитации футбольного табло для
 * отображения счёта в матче
 *
 * @author Петрушов Роман
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button button1;
    Button button2;
    Button button3;

    // начальное значение количества очков первой и второй команды
    private int firstTeam = 0;
    private int secondTeam = 0;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);

    }

    @Override
    /**
     * Анализируем, какая кнопка была нажата:
     * при нажатии кнопки {@code button1} инкрементируется счётчик первой команды {@code firstTeam};
     * при нажатии кнопки {@code button2} инкрементируется счётчик второй команды {@code secondTeam};
     * при нажатии кнопки {@code button3} обнуляются счётчики обеих команд
     *
     * @param v - вьюшка
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                firstTeam++;
                TextView counterViewFirstTeam = (TextView) findViewById(R.id.first_count);
                counterViewFirstTeam.setText(String.format(Locale.getDefault(), "%d", firstTeam));
                outputCountOfTeam(firstTeam, R.id.first_count);
                break;
            case R.id.button2:
                secondTeam++;
                TextView counterViewSecondTeam = (TextView) findViewById(R.id.second_count);
                counterViewSecondTeam.setText(String.format(Locale.getDefault(), "%d", secondTeam));
                outputCountOfTeam(secondTeam, R.id.second_count);
                break;
            case R.id.button3:
                firstTeam = 0;
                secondTeam = 0;
                TextView counterViewFirst_team = (TextView) findViewById(R.id.first_count );
                TextView counterViewSecond_team = (TextView) findViewById(R.id.second_count );
                counterViewFirst_team.setText(String.format(Locale.getDefault(), "%d", firstTeam));
                counterViewSecond_team.setText(String.format(Locale.getDefault(), "%d", secondTeam));
                outputCountOfTeam(firstTeam, R.id.first_count);
                outputCountOfTeam(secondTeam, R.id.second_count);
                break;
        }
    }


    /**
     * Восстанавливает состояние приложения после изменений Activity
     *
     * @param savedInstanceState - данные для восстановления приложения
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey("first_Team") && savedInstanceState.containsKey("second_Team")) {
            firstTeam = savedInstanceState.getInt("first_Team");
            secondTeam = savedInstanceState.getInt("second_Team");
            outputCountOfTeam(firstTeam, R.id.first_count);
            outputCountOfTeam(secondTeam, R.id.second_count);
        }
    }



    @Override

    /**
     * Сохраняет состояние Activity
     *
     * @param outState - временные данные в процессе работы Activity
     */
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("first_Team", firstTeam);
        outState.putInt("second_Team", secondTeam);
    }


    /**
     * Выводит количество очков на табло одной из выбранных команд
     *
     * @param command - количество очков выбранный команды
     * @param id    - идентификатор команды, которой нужно передать количество очков
     */
    public void outputCountOfTeam(int command, int id) {
        TextView counterView = (TextView) findViewById(id);
        counterView.setText(String.format(Locale.getDefault(), "%d", command));

        int arrayCount[] = {R.id.first_count, R.id.second_count, R.id.devider};

        if (firstTeam > 99 || secondTeam > 99) {
            setTextSizeOfCount(arrayCount, 60);
        } else {
            setTextSizeOfCount(arrayCount, 80);
        }
    }

    public void setTextSizeOfCount(int[] id, int textSize) {
        for (int i = 0; i < id.length; i++) {
            ((TextView) findViewById(id[i])).setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize);
        }
    }
}
